
Источник заданий - https://postgrespro.ru/education/courses/DEV1


--https://edu.postgrespro.ru/dev1-12/dev1_08_sql_func.html

--Напишите функцию, выдающую случайное время,равномерно распределенное в указанном отрезке.
--Начало отрезка задается временной отметкой (timestamptz),конец — либо временной отметкой, либо интервалом (interval)
create function rnd_timestamp(in start_time timestamptz, in end_time timestamptz, out timestamptz) as
	$$
		SELECT start_time + (end_time - start_time) * random();
	$$
volatile language sql;

create function rnd_timestamp(in start_time timestamptz, in inter interval, out timestamptz) as
	$$
		select rnd_timestamp(start_time, start_time + inter);
	$$
volatile language sql;

--В таблице хранятся номера автомобилей, введенные кое-как: встречаются как латинские,
--так и русские буквы в любом регистре; между буквами и цифрами могут быть пробелы.
--Считая, что формат номера «буква три-цифры две-буквы», напишите функцию, выдающую число уникальных номеров.
--Например, «К 123 ХМ» и «k123xm» считаются равными
create function normalizze(in num text, out text) as
	$$
		select upper(replace(num, ' ', ''));
	$$
immutable language sql;

--Напишите функцию, находящую корни квадратного уравнения
create function square_roots(in a int, in b int, in c int, out root1 int, out root2 int) as
	$$
		with discr as (select b ^ 2 - 4 * a * c val)
		select  case
					when val > 0 then -b + sqrt(val) / 2 / a
					when val = 0 then -b / 2 / a
					else null
				end,
				case
					when val > 0 then -b - sqrt(val) / 2 / a
					when val = 0 then null
					else null
				end
		from discr
	$$
immutable language sql;

--https://edu.postgrespro.ru/dev1-12/dev1_09_sql_proc.html

--В таблице authors имена, фамилии и отчества авторовпо смыслу должны быть уникальны, но это условие никак не проверяется.
--Напишите процедуру, удаляющую возможные дубликаты авторов. Чтобы необходимость в подобной процедуре не возникала,
--создайте ограничение целостности, которое не позволит появляться дубликатам в будущем
create procedure delete_rep_authors() AS
	$$
		delete from authors where author_id in (
			select author_id from (
					select author_id, row_number() over(partition by last_name, first_name, middle_name order by author_id) row_num from authors
				) tmp 
			where row_num <> 1
			);
	$$
language sql;

--В таблице хранятся вещественные числа (например, результаты каких-либо измерений).
--Напишите процедуру нормализации данных, которая умножает все числа на определенный коэффициент так,
--чтобы все значения попалив интервал от −1 до 1.Процедура должна возвращать выбранный коэффициент
create procedure normalizze(inout float) as 
	$$	
		with coef(val) as (select 1 / max(abs(num)) from t),
		update_t as (update t set num = num * val from coef)
		select val from coef;
	$$
language sql;

--https://edu.postgrespro.ru/dev1-12/dev1_10_sql_row.html

--Создайте функцию onhand_qty для подсчета имеющихсяв наличии книг.
--Функция принимает параметр составного типа books и возвращает целое число.
--Используйте эту функцию в представлении catalog_vв качестве «вычисляемого поля».
--Проверьте, что приложение отображает количество книг.
create function onhand_qty(in book books, out integer) as 
	$$
		select coalesce(sum(qty_change), 0) from operations o where o.book_id = book.book_id;
	$$
stable language sql;

--Создайте табличную функцию get_catalog для поиска книг.
--Функция принимает значения полей формы поиска(«имя автора», «название книги», «есть на складе»)
--и возвращает подходящие книги в формате catalog_v. Проверьте, что в «Магазине» начал работать поиск и просмотр
create function get_catalog(in author_name text, in book_title text, in in_stock boolean) returns table(display text, stock integer) as
	$$	
		select display, stock from (
			select display, 
					stock, 
					case 
						when in_stock = True and stock > 0 then True 
						when in_stock = True and stock <= 0 then False 
						when in_stock = False and stock > 0 then False 
						else True
					end to_display
			from catalog_v
			) tmp 
		where to_display=True and 
				replace(display, ' ', '') ~ replace(book_title, ' ', '') and 
				replace(display, ' ', '') ~ replace(author_name, ' ', '') and 
				replace(author_name, ' ', '') in (select last_name || first_name || coalesce(middle_name, '') from authors) and
				replace(book_title, ' ', '') in (select title from books);
	$$
stable language sql;

--Напишите функцию, переводящую строку, содержащую число в шестнадцатеричной системе, в обычное целое число
create function to_number(in str text, out num int) as 
	$$
		select convert(upper(str))
	$$
stable language sql;

--https://edu.postgrespro.ru/dev1-12/dev1_11_plpgsql_introduction.html


--Измените функцию book_name так, чтобы длина возвращаемого значения не превышала 45 символов.
--Если название книги при этом обрезается, оно должно завершаться на троеточие.
--Проверьте реализацию в SQL и в приложении;при необходимости добавьте книг с длинными названиями
--Снова измените функцию book_name так, чтобы избыточно длинное название уменьшалось на целое слово. Проверьте реализацию.
create function shorten(in str text, in delimeter text default '...', in max_symbols int default 45, out result text) as 
	$$
		declare 
			str_length int := length(str);
			last_symb text := ' ';
		begin
			if str_length > max_symbols then
				while str_length > max_symbols - length(delimeter)
				loop
					last_symb = substr(str, str_length);
					str = substr(str, 1, str_length - 1);
					str_length = str_length - 1;
				end loop;
				if str ~ ' ' then
					while last_symb <> ' '
					loop
						raise notice '%', last_symb;
						last_symb = substr(str, str_length);
						str = substr(str, 1, str_length - 1);
						str_length = str_length - 1;
					end loop;

				end if;
				result = concat(str, delimeter);
			else
				result = str;
			end if;
		end;
	$$
immutable language plpgsql;

create function book_name(in book_id int, in title text, out text) as 
	$$
		select shorten(title || ' ' || string_agg(author_name(last_name, first_name, middle_name), ', ' order by seq_num))
		from authorship inner join authors using(author_id) where authorship.book_id = book_id
	$$
stable language sql;

--Напишите PL/pgSQL-функцию, которая возвращает строку заданной длины из случайных символов.
create function rnd_integer(in low int, in high int) returns int as 
	$$
		begin
		RETURN floor(random() * (high-low + 1) + low);
		end;
	$$
language plpgsql strict;

create function rnd_text(in len int, in list_of_chars text DEFAULT 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюяABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_0123456789', out result text) as
	$$
		declare
			len_list_of_chars int = length(list_of_chars);
			rand_indx int;
		begin
			result = '';
			for i in 1 .. len
			loop
				rand_indx = rnd_integer(1, len_list_of_chars);
				result = result || substr(list_of_chars, rand_indx, 1);
			end loop;
		end;
	$$
language plpgsql;

--Задача про игру в «наперстки».В одном из трех наперстков спрятан выигрыш.
--Игрок выбирает один из этих трех. Ведущий убирает один из двух оставшихся наперстков (обязательно пустой)
--и дает игроку возможность поменять решение, то есть выбрать второй из двух оставшихся.
--Есть ли смысл игроку менять выбор или нет смысла менять первоначальный вариант?
--Задание: используя PL/pgSQL, посчитайте вероятность выигрыша и для начального выбора, и для измененного.
do $$
	declare
		cups text = '123';
		new_cups text;
		right_answer int;
		first_choice int;
		second_choice int;
		to_remove int;
		first_points int = 0;
		second_points int = 0;
	begin
		for i in 1 .. 100000
		loop
			right_answer = rnd_integer(1, 3);
			first_choice = rnd_integer(1, 3);
			if first_choice = right_answer then
				first_points = first_points + 1;
			end if;
			if first_choice <> right_answer then
				new_cups = right_answer::text || first_choice::text;
			else new_cups = first_choice::text || rnd_text(1, list_of_chars => replace(cups, first_choice::text, ''));
			end if;
			second_choice = rnd_text(1, new_cups);
			if second_choice::int = right_answer then
				second_points = second_points + 1;
			end if;
		end loop;

		if first_points = second_points then
			raise notice 'ничья, %:%', first_points, second_points;
		elseif first_points > second_points then
			raise notice 'начальный, %:%', first_points, second_points;
		else raise notice 'измененный, %:%', first_points, second_points;
		end if;


	end;
$$ language plpgsql;

--https://edu.postgrespro.ru/dev1-12/dev1_12_plpgsql_queries.html

--Напишите функцию add_author для добавления новых авторов.
--Функция должна принимать три параметра (фамилия, имя, отчество) и возвращать идентификатор нового автора
create function add_author(in last_name text, in first_name text, in middle_name text, out author_id int) as 
	$$
		begin
			insert into authors(last_name, first_name, middle_name) values (last_name, first_name, middle_name) returning authors.author_id into author_id;
		end;
	$$
language plpgsql volatile;

--Напишите функцию buy_book для покупки книги.
--Функция принимает идентификатор книги и уменьшает количество таких книг на складе на единицу.
--Возвращаемое значение отсутствует
create function buy_book(book_id integer) RETURNS void as 
	$$
		begin
			insert into operations(book_id, qty_change) values (book_id, -1);
		end;
	$$
VOLATILE LANGUAGE plpgsql;

--https://edu.postgrespro.ru/dev1-12/dev1_13_plpgsql_cursors.html

--Измените функцию book_name: если у книги больше двух авторов,
--то в названии указываются только первые дваи в конце добавляется «и др.»
create function book_name(in book_id int, in title text, out result text) as 
	$$
		declare
			cnt int = 0;
			rec record;
		begin
			result = shorten(title) || ' ';
			for rec in (
				select last_name, first_name, middle_name, seq_num 
				from authorship inner join authors using(author_id) 
				where authorship.book_id = book_name.book_id 
				order by seq_num
				)
			loop
				cnt = cnt + 1;
				if cnt <= 2 then
					result = result || rec.last_name || ' ' || rec.first_name || ' ' || coalesce(rec.middle_name, '') || ' ';
				end if;
			end loop;
			if cnt > 2 then
				result = result || ' и др.';
			end if;
		end;
	$$
language plpgsql;

--Попробуйте написать функцию book_name на SQL.Какой вариант нравится больше — PL/pgSQL или SQL?
create function book_name(in book_id int, in title text, in max_authors int default 2, out result text) as 
	$$

		select shorten(title) || '. ' || 
			string_agg(author_name(last_name, first_name, middle_name), ', ' order by seq_num) 
			filter (where seq_num <= max_authors) || 
			case when max(seq_num) > max_authors then ' и др.' else '' end
		from authorship inner join authors using(author_id) 
		where authorship.book_id = book_name.book_id 

	$$
language sql;

--Требуется распределить расходы на электроэнергиюпо отделам компании пропорционально количеству сотрудников
--(перечень отделов находится в таблице).Напишите функцию, которая примет общую сумму расходов
--и запишет распределенные расходы в строки таблицы.Числа округляются до копеек;
--сумма расходов всех отделов должна в точности совпадать с общей суммой.
CREATE TABLE depts(    
	id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,    
	employees integer,    
	expenses numeric(10,2)
);
create function distribute_expenses(in amount numeric) RETURNS void as 
	$$
		declare
			sum_employees int;
			cur cursor for select employees from depts for update;
		begin
			select sum(employees) from depts into sum_employees;
			for rec in cur
			loop
				raise notice '%', rec.employees;
				raise notice '%', sum_employees;
				raise notice '%', amount;
				update depts set expenses = round(rec.employees::numeric / sum_employees::numeric * amount, 2) where current of cur;
			end loop;
		end;
	$$
language plpgsql;


--Напишите табличную функцию, имитирующую сортировку слиянием. Функция принимает две курсорные переменные;
--оба курсора уже открыты и возвращают упорядоченные по неубыванию целые числа.
--Требуется выдать общую упорядоченную последовательность чисел из обоих источников
create function merge(c1 refcursor, c2 refcursor) RETURNS SETOF integer as 
	$$
		declare
			c1_rec int;
			c2_rec int;
		begin
			fetch c1 into c1_rec;
			fetch c2 into c2_rec;
			loop
			
				EXIT WHEN c1_rec IS NULL AND c2_rec IS NULL;
				if c1_rec <= c2_rec or c2_rec is NULL then
					return next c1_rec;
					fetch c1 into c1_rec;
				else 
					return next c2_rec;
					fetch c2 into c2_rec;
				end if;

			end loop;
		end;
	$$
language plpgsql;

--https://edu.postgrespro.ru/dev1-12/dev1_14_plpgsql_dynamic.html

--Измените функцию get_catalog так, чтобы запроск представлению catalog_v формировался динамическии содержал
--условия только на те поля, которые заполненына форме поиска в «Магазине».
--Убедитесь, что реализация не допускает возможности внедрения SQL-кода
create function get_catalog_dynamic(in author_name text, in book_title text, in in_stock boolean) returns table(display text, stock integer) as
	$$	
		declare
			cmd text = 'select display, stock from (
				select display, 
						stock, 
						case 
							when %L = True and stock > 0 then True 
							when %L = True and stock <= 0 then False 
							when %L = False and stock > 0 then False 
							else True
						end to_display
				from catalog_v
				) tmp 
			where to_display=True and display ~ %L and display ~ %L';
		begin
			raise log '%', cmd;
			return query execute format(cmd, in_stock, in_stock, in_stock, book_title, author_name);
		end;
	$$
language plpgsql;

--Создайте функцию, которая возвращает строки матричного отчета по функциям в базе данных.
--Столбцы должны содержать имена владельцев функций,строки — названия схем,
--а ячейки — количество функций данного владельца в данной схеме. Как можно вызвать такую функцию?
create function matrix() returns setof record as 
	$$
		declare
			owner text;
			query_start text = 'SELECT pronamespace::regnamespace::text AS schema,       
				COUNT(*) AS total
				';
			query_end text = 'FROM pg_proc 
							GROUP BY pronamespace::regnamespace 
							ORDER BY schema';
			query_middle text = '';
		begin
			for owner in (select distinct proowner::regrole::text owner from pg_proc ORDER BY 1)
			loop
				query_middle = query_middle || format(',SUM(CASE WHEN proowner::regrole::text = %L THEN 1 ELSE 0 END)::bigint %I ', owner, owner);
			end loop;
			return query execute query_start || query_middle || query_end;
		end;
	$$
language plpgsql;	

CREATE FUNCTION matrix_call() RETURNS text
AS $$
DECLARE
    cmd text;
    r record;
BEGIN
    cmd := 'SELECT * FROM matrix() AS m(
        schema text, total bigint';

    FOR r IN (SELECT DISTINCT proowner::regrole::text AS owner FROM pg_proc ORDER BY 1)
    LOOP
        cmd := cmd || format(', %I bigint', r.owner);
    END LOOP;
    cmd := cmd || E'\n)';

    RAISE NOTICE '%', cmd;
    RETURN cmd;
END;
$$ STABLE LANGUAGE plpgsql;

--https://edu.postgrespro.ru/dev1-12/dev1_15_plpgsql_arrays.html

--Создайте функцию add_book для добавления новой книги. Функция должна принимать два параметра —название книги
--и массив идентификаторов авторов —и возвращать идентификатор новой книги.
--Проверьте, что в приложении появилась возможность добавлять книги.
create FUNCTION add_book(in title text, in authors integer[], out book_id int) as 
	$$
		declare
		begin
			insert into books(title) values(title) returning books.book_id into add_book.book_id;
			for i in array_lower(authors, 1) .. array_upper(authors, 1) loop
				insert into authorship values (book_id, authors[i], i); 
			end loop;
		end;
	$$
volatile language plpgsql;

--Реализуйте функцию map, принимающую два параметра:массив вещественных чисел и название вспомогательной функции,
--принимающей один параметр вещественного типа. Функция возвращает массив,
--полученный из исходного применением вспомогательной функции к каждому элементу.
create function map(in numbers anyarray, in func text, in array_type anyelement default null) returns anyarray as 
	$$
		declare
			num array_type%type;
			result numbers%type default null;
		begin
			if array_lower(numbers, 1) is not null then
				for i in array_lower(numbers, 1) .. array_upper(numbers, 1) loop
					execute format('select %I(%L)', func, numbers[i]) into num;
					result[i] = num;

				end loop;
			end if;
			return result;
		end;
	$$
immutable language plpgsql;

--Реализуйте функцию reduce, принимающую два параметра:массив вещественных чисел и название вспомогательной функции,
--принимающей два параметра вещественного типа. Функция возвращает вещественное число,
--полученное последовательной сверткой массива слева направо.
--Сделайте функции map и reduce полиморфными
create function reduce(in numbers anyarray, in func text, in array_type anyelement default null) returns anyelement as 
	$$
		declare
			prev_res array_type%type;
		begin
			prev_res = numbers[array_lower(numbers, 1)] ;
			for i in array_lower(numbers, 1) + 1 .. array_upper(numbers, 1) loop
				execute format('select %I($1, $2)', func) using prev_res, numbers[i] into prev_res;
				numbers[i] = num;
			end loop;
			return prev_res;
		end;
	$$
immutable language plpgsql;



--Если при добавлении новой книги указать одного и того же автора несколько раз, произойдет ошибка.
--Измените функцию add_book: перехватите ошибку нарушения уникальности и вместо нее
--вызовите ошибкус понятным текстовым сообщением
create FUNCTION add_book(in title text, in authors integer[], out book_id int) as 
	$$
		declare
		begin
			insert into books(title) values(title) returning books.book_id into add_book.book_id;
			for i in array_lower(authors, 1) .. array_upper(authors, 1) loop
				insert into authorship values (book_id, authors[i], i); 
			end loop;
		exception
		when unique_violation then 
			raise notice '%: %', sqlstate, sqlerrm;
		end;
	$$
volatile language plpgsql;


CREATE PROCEDURE baz()
AS $$
BEGIN
    PERFORM 1 / 0;
END;
$$ LANGUAGE plpgsql;

--Сравните стеки вызовов, получаемые конструкциями GET STACKED DIAGNOSTICS с элементом
--pg_exception_contextи GET [CURRENT] DIAGNOSTICS с элементом pg_context.
CREATE OR REPLACE PROCEDURE bar()
AS $$
DECLARE
    msg text;
    ctx text;
BEGIN
    CALL baz();
EXCEPTION
    WHEN others THEN
        GET CURRENT DIAGNOSTICS
             ctx = pg_context;
        RAISE NOTICE E'\nОшибка: \nСтек ошибки:\n%\n', ctx;
END;
$$ LANGUAGE plpgsql;

CREATE PROCEDURE foo()
AS $$
BEGIN
    CALL bar();
END;
$$ LANGUAGE plpgsql;


--https://edu.postgrespro.ru/dev1-12/dev1_17_plpgsql_triggers.html


--Создайте триггер, обрабатывающий обновление поля onhand_qty представления catalog_v
CREATE OR REPLACE FUNCTION catalog_v_update() RETURNS trigger
AS $$
DECLARE
    book_id int;
    old_stock int;
    old_title text = split_part(OLD.display, '.', 1);
BEGIN
    BEGIN
        SELECT books.book_id INTO STRICT book_id
        FROM books
        WHERE books.title = old_title;
        select onhand_qty(ROW(book_id, old_title)::books) into strict old_stock;
    EXCEPTION
        WHEN no_data_found THEN
            RAISE EXCEPTION 'Книга % отсутствует', title;
    END;
    insert into operations(book_id, qty_change) values (book_id, NEW.stock - coalesce(old_stock, 0));
    RETURN NEW;
END;
$$ volatile LANGUAGE plpgsql;

CREATE TRIGGER catalog_v_upd_trigger
INSTEAD OF UPDATE ON catalog_v
FOR EACH ROW EXECUTE FUNCTION catalog_v_update();

--Обеспечьте выполнение требования согласованности: количество книг на складе не может быть отрицательным
--(нельзя купить книгу, которой нет в наличии). Внимательно проверьте правильность реализации,
--учитывая, что с приложением могут одновременно работать несколько пользователей.
CREATE OR REPLACE FUNCTION oparations_update() RETURNS trigger
AS $$
DECLARE
    new_onhand_qty int;
BEGIN
    BEGIN
        select sum(qty_change) into strict new_onhand_qty
        from operations o
        where o.book_id = NEW.book_id;
    exception
    	when no_data_found then
    	raise exception 'there are no operations with such a book'; 
    END;
    update books set onhand_qty = new_onhand_qty where books.book_id = NEW.book_id;
    RETURN NEW;
END;
$$ volatile LANGUAGE plpgsql;

create trigger operations_update_trigger after update or insert or delete on operations for each row execute function oparations_update();


--Напишите триггер, увеличивающий счетчик (поле version)на единицу при каждом изменении строки.
--При вставке новой строки счетчик должен устанавливаться в единицу.Проверьте правильность работы
CREATE TABLE text_with_version(
    id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    s text,
    version integer
);

create or replace function text_with_version_update() returns trigger AS
	$$
		begin
			if TG_OP = 'INSERT' then 
				NEW.version =  1;
			elseif TG_OP = 'UPDATE' then 
				NEW.version = OLD.version + 1;
			end if;
			return new;
		end;
	$$
volatile language plpgsql;

create trigger text_with_version_update_trigger before update or insert on text_with_version for each row execute function text_with_version_update();

--Даны таблицы заказов (orders) и строк заказов (lines).Требуется выполнить денормализацию:
--автоматически обновлять сумму заказа в таблице orders при изменении строк в заказе.
--Создайте необходимые триггеры с использованием переходных таблиц для минимизации операций обновления
CREATE TABLE orders (    
	id int PRIMARY KEY,    
	total_amount numeric(20,2) NOT NULL DEFAULT 0
	);
CREATE TABLE lines (   
	id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,   
	order_id int NOT NULL REFERENCES orders(id),   
	amount numeric(20,2) NOT NULL
	);

create or replace function lines_update() returns trigger as 
	$$
		declare
		old_amount int;
		begin
			if TG_OP = 'INSERT' then 
			old_amount = 0;
			else old_amount = old.amount;
			end if;
			update orders set total_amount = total_amount - old_amount + new.amount where id = new.order_id;
			return new;
		end;

	$$
volatile language plpgsql;

create trigger lines_update_trigger after update or insert on lines for each row execute function lines_update();

